"use strict";




var express = require('express');
var mongoose = require('mongoose');
var app = module.exports = express();

// connect to Mongo when the app initializes
mongoose.connect('mongodb://localhost/mongo');

var db = mongoose.connection;
db.on('error', function() {console.log('#########connection error to mongodb#########')});

//Config
app.configure(function(){
   app.set('port', process.env.PORT || 3000);
   app.set('views', './app');
   app.engine('.html', require('ejs').renderFile);
   app.set('view engine', 'html');
   app.use(express.json());
   app.use(express.urlencoded());
   app.use(express.methodOverride());
   app.use(app.router);
   app.use(express.favicon());
   app.use(express.logger('dev'));
   app.use(express.static('./app'));
});


// Angular Routes
var routes = require('./routes/index.js');
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

//RESTful API
var api = require('./routes/api.js');
app.post('/thread', api.post);
app.get('/thread/:title.:format?', api.show);
app.get('/thread', api.list);

//Start server
var server = app.listen(app.get('port'), function(){
    console.log("Server listening on port %d", server.address().port);
});





